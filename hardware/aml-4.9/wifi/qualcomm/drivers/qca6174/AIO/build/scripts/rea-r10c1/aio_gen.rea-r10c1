#!/bin/bash
# Add r32a-r103 specific patches

# subverion
BRD_TYPE_VER=rea-r10c1

#
# For each board type,
# determine which components should be downloaded.
# The settings here will overwrite the default in aio_gen_bit.sh.
# 
# ONLY add/enable those components need download.
#
# 1. patch files
DOWNLOAD_PATCH=y

# 2. kernel
DOWNLOAD_KERNEL_BACKPORT_3_12=y

# 3. WLAN/BT host drivers
DOWNLOAD_DRIVER_WLAN_HOST=n

# 4. APPs
DOWNLOAD_APP_WLAN_WPA_SUPPLICANT_8=y
DOWNLOAD_APP_WLAN_LIBNL_3_2_25=y

CFG80211_VERSION="v3.18.1"
CFG80211_BASE_NAME="backports-3.18.1-1"

BOARD_TYPE_PREFIX=${INPUT_BOARD_TYPE/-/_}

# The format of array should be composed of "BOARD_TYPE_PREFIX + BOARD_TYPE_POSTFIX"
declare -a rea_r10c1_aio_patch_arr=(
"1001-cus-Changes-for-qcacld-2.0-LEA-Branch.patch"
"1003-cld-hif-rx.patch"
"1004-cld-hif-tx.patch"
"1005-cld-ramdump.patch"
)

# Please add the correct link on CAF if related folder released
#BOARD_TYPE_AIO_PATCH_CAF=
BOARD_TYPE_AIO_PATCH_CAF="https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/rea-r10c1"


#### use git to download the WLAN host drivers


git clone git://codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0 -b caf-wlan/CNSS.LEA.NRT_1.0
cd qcacld-2.0 && git reset --hard   32e97738e4acc88639ec4b74332616961764bd36 && cd ..
mv  qcacld-2.0 ${AIO_TOP}/drivers/qcacld-new
echo "qcacld driver download done"


#### Download backports patches from CAF
PATCH_PATH=${AIO_TOP}/drivers/patches/${BRD_TYPE_VER}
COMMONPATCH_PATH=${AIO_TOP}/drivers/patches/
mkdir -p ${PATCH_PATH}
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=504152d28e36e815ad076c20e3fd8e279789a0d1 -O ${PATCH_PATH}/0001-cfg80211-Add-AP-stopped-interface.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=4392e1b1f1a60651065e271a0facbcd1ae47f0c3 -O ${PATCH_PATH}/0002-cfg80211-Reset-beacon-interval-when-stop-AP-fails.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=4df810901c1fae517181dd2df965ab3672a36d5f -O ${PATCH_PATH}/0003-Revert-cfg80211-mac80211-disconnect-on-suspend.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=8dd59f4306c6dd309b976e64be82a83ab624a40f -O ${PATCH_PATH}/0004-cfg80211-Add-new-wiphy-flag-WIPHY_FLAG_DFS_OFFLOAD.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=0e078ec39e55d7791db7835c0b83fb0547286e5d -O ${PATCH_PATH}/0005-cfg80211-export-regulatory_hint_user-API.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/patch/?id=389b3f4f0d3202fc9cf36713599f19b7e9919fa1 -O ${PATCH_PATH}/0006-mac80211-implement-HS2.0-gratuitous-ARP-unsolicited.patch
wget https://source.codeaurora.org/quic/la/kernel/msm-3.18/plain/net/wireless/db.txt  -O ${PATCH_PATH}/0007-add-db.txt
wget https://source.codeaurora.org/quic/la/platform/vendor/qcom-opensource/wlan/qcacld-2.0/patch/CORE/SME/src/csr/csrApiRoam.c?id=f7b19d5a2c0548e06312296219bf9cc4b5901a25 -O ${COMMONPATCH_PATH}/925-fix-sap-11nght20-with-VHT-IE.patch

### Download android x86 M common patch from CAF
M_COMMONPATCH_PATH=${AIO_TOP}/drivers/patches/android_x86_M/WLAN
mkdir -p ${M_COMMONPATCH_PATH}

#ANDROID_X86_M_COMMON_PATCH_CAF="https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/M/WLAN"
wget https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/M/WLAN/0001.WL-device.patch -O ${M_COMMONPATCH_PATH}/0001.WL-device.patch
wget https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/M/WLAN/0002.WL-frameworks.patch -O ${M_COMMONPATCH_PATH}/0002.WL-frameworks.patch
wget https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/M/WLAN/0003.WL-hardware.patch -O ${M_COMMONPATCH_PATH}/0003.WL-hardware.patch

### Download android x86 L common patch from CAF
wget https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/WLAN/0004.WL-kernel.patch -O ${M_COMMONPATCH_PATH}/0004.WL-kernel.patch
wget https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/x86-android/WLAN/0005.WL-3.10-kernel-tcp-rx-tput-fix.patch -O ${M_COMMONPATCH_PATH}/0005.WL-3.10-kernel-tcp-rx-tput-fix.patch

echo "net.ipv4.tcp_use_userconfig = 1" > ${M_COMMONPATCH_PATH}/sysctl.conf
echo "net.ipv4.tcp_delack_seg = 10" >> ${M_COMMONPATCH_PATH}/sysctl.conf

echo "#! /bin/bash" > ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "echo \"Start to patch files\"" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "patch -p1 --no-backup-if-mismatch < 0001.WL-device.patch" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "patch -p1 --no-backup-if-mismatch < 0002.WL-frameworks.patch" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "patch -p1 --no-backup-if-mismatch < 0003.WL-hardware.patch" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "patch -p1 --no-backup-if-mismatch < 0004.WL-kernel.patch" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "patch -p1 --no-backup-if-mismatch < 0005.WL-3.10-kernel-tcp-rx-tput-fix.patch" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "cp sysctl.conf out/target/product/x86/system/etc/" >> ${M_COMMONPATCH_PATH}/patch_wl.sh
echo "echo \"Done! patch completed!\"" >> ${M_COMMONPATCH_PATH}/patch_wl.sh

echo "backports 3.18 patches download done"
