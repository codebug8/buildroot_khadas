export AIO_VER=1.4.2.rea-r10c1.008.001
export BOARD_TYPE=rea-r10c1
export HOST_DRIVER_VERSION=4.5.22.7
export BLUETOOTHSTACK=bluedroid
export BOARD_TYPE_AIO_PATCH_CAF=https://source.codeaurora.org/patches/external/wlan/fixce/3rdparty/patches/rea-r10c1
export IF_TYPE=SDIO
export ENG_PATCH=1
