### Install essential packages

```
$ sudo apt-get update
$ sudo apt-get install -y repo git-core gitk git-gui gcc-arm-linux-gnueabihf u-boot-tools device-tree-compiler gcc-aarch64-linux-gnu mtools parted libudev-dev \
		libusb-1.0-0-dev python-linaro-image-tools linaro-image-tools autoconf autotools-dev libsigsegv2 m4 intltool libdrm-dev curl sed make binutils \
		build-essential gcc g++ bash patch gzip bzip2 perl tar cpio python unzip rsync file bc wget libncurses5 libqt4-dev libglib2.0-dev libgtk2.0-dev \
		libglade2-dev cvs git mercurial rsync openssh-client subversion asciidoc w3m dblatex graphviz python-matplotlib libc6:i386 libssl-dev texinfo liblz4-tool genext2fs \
		zlib1g:i386 lib32stdc++6
```

### Install toolchains

Install `gcc-linaro-6.3.1-2017.02-x86_64_aarch64-linux-gnu`:

```
$ sudo mkdir -p /opt/toolchains
$ wget https://releases.linaro.org/components/toolchain/binaries/6.3-2017.02/aarch64-linux-gnu/gcc-linaro-6.3.1-2017.02-x86_64_aarch64-linux-gnu.tar.xz -P /tmp
$ sudo tar xJvf /tmp/gcc-linaro-6.3.1-2017.02-x86_64_aarch64-linux-gnu.tar.xz -C /opt/toolchains
```

Install `gcc-linaro-aarch64-none-elf-4.8-2013.11_linux`:

```
$ wget https://releases.linaro.org/archive/13.11/components/toolchain/binaries/gcc-linaro-aarch64-none-elf-4.8-2013.11_linux.tar.xz -P /tmp
$ sudo tar xJvf /tmp/gcc-linaro-aarch64-none-elf-4.8-2013.11_linux.tar.xz -C /opt/toolchains
```

Install `gcc-linaro-arm-none-eabi-4.8-2014.04_linux`:

```
$ wget https://releases.linaro.org/archive/14.04/components/toolchain/binaries/gcc-linaro-arm-none-eabi-4.8-2014.04_linux.tar.xz -P /tmp
$ sudo tar xJvf /tmp/gcc-linaro-arm-none-eabi-4.8-2014.04_linux.tar.xz -C /opt/toolchains
```
Install `gcc-arm-none-eabi-6-2017-q1-update-linux`:

```
$ wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/6_1-2017q1/gcc-arm-none-eabi-6-2017-q1-update-linux.tar.bz2 -P /tmp
$ sudo tar xjvf /tmp/gcc-arm-none-eabi-6-2017-q1-update-linux.tar.bz2 -C /opt/toolchains
```

Add to environment path: add `export PATH=$PATH:/opt/toolchains/gcc-linaro-aarch64-none-elf-4.8-2013.11_linux/bin:/opt/toolchains/gcc-linaro-arm-none-eabi-4.8-2014.04_linux/bin:/opt/toolchains/gcc-arm-none-eabi-6-2017-q1-update/bin:/opt/toolchains/gcc-linaro-6.3.1-2017.02-x86_64_aarch64-linux-gnu/bin` to `~/.bashrc`

### Build the image

```
$ cd /path/to/buildroot
$ source setenv.sh      ## Choose `1` and press `Enter` or press `Enter` directly
$ make
```

### Build the u-boot

Do modifications to the u-boot source code(`bootloader/uboot-repo/bl33`) you want. And rebuild the uboot:

```
$ source setenv.sh      ## Choose `1` and press `Enter` or press `Enter` directly
$ make uboot-rebuild
$ make
```
After build will regenerate the image, you can flash the new image.

### Build the kernel

Do modifications to the kernel source code(`kernel/aml-4.9/`) you want. And rebuild the kernel:

```
$ source setenv.sh      ## Choose `1` and press `Enter` or press `Enter` directly
$ make linux-rebuild
$ make
```

After build will regenerate the image, you can flash the new image.
